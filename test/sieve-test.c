/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of sieve-of-eratosthenes.
 *
 * sieve-of-eratosthenes is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sieve-of-eratosthenes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sieve-of-eratosthenes.  If not, see
 * <https://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sieve.h>


#define ARR_LEN(arr) (sizeof(arr) / sizeof(*(arr)))


static
size_t const
primes[]
  = {
        2,   3,   5,   7,  11,  13,  17,  19,
       23,  29,  31,  37,  41,  43,  47,  53,
       59,  61,  67,  71,  73,  79,  83,  89,
       97, 101, 103, 107, 109, 113, 127
    };

static
size_t const
max[]
  = {
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      11, 95, 96, 97, 98, 126, 127, 128
    };

static
size_t const
count[]
  = {
      0, 0, 1, 2, 2, 3, 3, 4, 4, 4, 4,
      5, 24, 24, 25, 25, 30, 31, 31
    };


static
int
prime_to_array(size_t p, void *arg);

static
int
stop_n(size_t p, void *arg);


/**********************
 * exported functions *
 **********************/

int
main()
{
  size_t i, j, *it, arr[ARR_LEN(primes)];


  printf("1..%zu\n", 2 + ARR_LEN(max));

  if (0 == strcmp(libsieve_version, libsieve_header_version))
  {
    puts("ok 1 - version constant and macro are equal");
  }
  else
  {
    puts("not ok 1 - version constant and macro are not equal");
  }

  i = 5;
  sieve(128, &stop_n, &i);
  if (0 == i)
  {
    puts("ok 2 - stops when function returns 1");
  }
  else
  {
    puts("not ok 2 - doesn't stop when function returns 1");
  }

  for (i = 0; i < ARR_LEN(max); ++i)
  {
    it = arr;
    sieve(max[i], &prime_to_array, &it);
    if (arr + count[i] != it)
    {
      printf("not ok %zu - when given %zu as maximum, provides %zu primes, "
             "but should have provided %zu\n", 3 + i, max[i], it - arr,
             count[i]);
    }
    else
    {
      for (j = 0; j < count[i]; ++j)
      {
        if (primes[j] != arr[j])
        {
          break;
        }
      }

      if (count[i] != j)
      {
        printf("not ok %zu - when given %zu as maximum, gave %zu as a prime, "
               "but should have given %zu\n", 3 + i, max[i], arr[j],
               primes[j]);
      }
      else
      {
        printf("ok %zu - provides %zu correct primes when given %zu as "
               "maximum\n", 3 + i, count[i], max[i]);
      }
    }
  }

  return 0;
}


/*******************
 * local functions *
 *******************/

int
prime_to_array(size_t p, void *arg)
{
  size_t **it;


  it = arg;

  **it = p;
  ++*it;

  return 0;
}

int
stop_n(size_t p, void *arg)
{
  size_t *remaining;


  (void) p;

  remaining = arg;

  --*remaining;

  return 0 == *remaining;
}
