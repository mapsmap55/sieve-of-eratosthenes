/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of sieve-of-eratosthenes.
 *
 * sieve-of-eratosthenes is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sieve-of-eratosthenes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sieve-of-eratosthenes.  If not, see
 * <https://www.gnu.org/licenses/>.
 */


#ifndef LIBSIEVE_BITVECTOR_H
# define LIBSIEVE_BITVECTOR_H


# include <stddef.h>


# ifdef __cplusplus
extern "C"
{
# endif


struct
bitvector
{
  size_t
  length;

  unsigned int *
  data;
};


# ifdef __GNUC__
#  define LIBSIEVE_PURE __attribute__ ((pure))
# else
#  define LIBSIEVE_PURE
# endif


int
bitvector_init(struct bitvector *bv, size_t min_length);

void
bitvector_destroy(struct bitvector *bv);

size_t
bitvector_length(struct bitvector const *bv)
  LIBSIEVE_PURE;

int
bitvector_get(struct bitvector const *bv, size_t index)
  LIBSIEVE_PURE;

void
bitvector_set(struct bitvector const *bv, size_t index);

void
bitvector_unset(struct bitvector const *bv, size_t index);

void
bitvector_flip(struct bitvector const *bv, size_t index);

void
bitvector_set_all(struct bitvector const *bv);

void
bitvector_unset_all(struct bitvector const *bv);

void
bitvector_flip_all(struct bitvector const *bv);


# undef LIBSIEVE_PURE


# ifdef __cplusplus
}
# endif


#endif
