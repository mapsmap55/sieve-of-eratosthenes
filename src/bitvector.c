/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of sieve-of-eratosthenes.
 *
 * sieve-of-eratosthenes is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sieve-of-eratosthenes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sieve-of-eratosthenes.  If not, see
 * <https://www.gnu.org/licenses/>.
 */


#include "bitvector.h"

#include <stdlib.h>
#include <limits.h>


#define BITS(expr) (CHAR_BIT * sizeof(expr))
#define CEIL_DIV(num, den) (((num) + (den) - 1) / (den))


int
bitvector_init(struct bitvector *bv, size_t min_length)
{
  bv->length = CEIL_DIV(min_length, BITS(*bv->data));
  bv->data = malloc(bv->length * sizeof(*bv->data));
  if (NULL == bv->data)
  {
    return 1;
  }

  return 0;
}

void
bitvector_destroy(struct bitvector *bv)
{
  free(bv->data);
}

size_t
bitvector_length(struct bitvector const *bv)
{
  return bv->length * BITS(*bv->data);
}

int
bitvector_get(struct bitvector const *bv, size_t index)
{
  unsigned int mask;
  unsigned int const *elem;


  mask = 1U << (index % BITS(*bv->data));
  elem = &bv->data[index / BITS(*bv->data)];

  return 0 != (*elem & mask);
}

void
bitvector_set(struct bitvector const *bv, size_t index)
{
  unsigned int mask, *elem;


  mask = 1U << (index % BITS(*bv->data));
  elem = &bv->data[index / BITS(*bv->data)];

  *elem |= mask;
}

void
bitvector_unset(struct bitvector const *bv, size_t index)
{
  unsigned int mask, *elem;


  mask = 1U << (index % BITS(*bv->data));
  elem = &bv->data[index / BITS(*bv->data)];

  *elem &= ~mask;
}

void
bitvector_flip(struct bitvector const *bv, size_t index)
{
  unsigned int mask, *elem;


  mask = 1U << (index % BITS(*bv->data));
  elem = &bv->data[index / BITS(*bv->data)];

  *elem ^= mask;
}

void
bitvector_set_all(struct bitvector const *bv)
{
  size_t i;


  for (i = 0; i < bv->length; ++i)
  {
    bv->data[i] = ~0U;
  }
}

void
bitvector_unset_all(struct bitvector const *bv)
{
  size_t i;


  for (i = 0; i < bv->length; ++i)
  {
    bv->data[i] = 0U;
  }
}

void
bitvector_flip_all(struct bitvector const *bv)
{
  size_t i;


  for (i = 0; i < bv->length; ++i)
  {
    bv->data[i] = ~bv->data[i];
  }
}
