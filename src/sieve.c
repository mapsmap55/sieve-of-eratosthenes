/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of sieve-of-eratosthenes.
 *
 * sieve-of-eratosthenes is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sieve-of-eratosthenes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sieve-of-eratosthenes.  If not, see
 * <https://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "sieve.h"

#include <stddef.h>

#include "bitvector.h"


char const * const
libsieve_version
  = PACKAGE_VERSION;


static
# ifdef __GNUC__
__attribute__ ((const))
# endif
size_t
floor_sqrt(size_t n);

static
void
unset_multiples(struct bitvector *primes, size_t n, size_t max, int n_mod_6);


/**********************
 * exported functions *
 **********************/

int
sieve(size_t max, int (*process_prime)(size_t, void *), void *arg)
{
  struct bitvector primes;
  size_t i, n, sqrt_max;
  int inc;


  if (2 > max)
  {
    return 0;
  }
  if (3 > max)
  {
    (*process_prime)(2, arg);

    return 0;
  }
  if (5 > max)
  {
    if (!(*process_prime)(2, arg))
    {
      (*process_prime)(3, arg);
    }

    return 0;
  }

  if (bitvector_init(&primes, (max - 1) / 3))
  {
    return 1;
  }

  if ((*process_prime)(2, arg) || (2 < max && (*process_prime)(3, arg)))
  {
    bitvector_destroy(&primes);

    return 0;
  }

  bitvector_set_all(&primes);

  sqrt_max = floor_sqrt(max);

  i = 0;
  inc = 2;
  for (n = 5; n - 1 < max; ++i, n += inc, inc = 6 - inc)
  {
    if (bitvector_get(&primes, i))
    {
      if ((*process_prime)(n, arg))
      {
        break;
      }

      if (n <= sqrt_max)
      {
        unset_multiples(&primes, n, max, inc - 3);
      }
    }
  }

  bitvector_destroy(&primes);

  return 0;
}


/*******************
 * local functions *
 *******************/

size_t
floor_sqrt(size_t n)
{
  size_t guess, inc;


  if (4 > n)
  {
    return (0 == n) ? 0 : 1;
  }

  guess = n / 2;
  inc = guess / 2;

  while (guess > n / guess || guess + 1 <= n / (guess + 1))
  {
    if (guess > n / guess)
    {
      guess -= inc;
    }
    else
    {
      guess += inc;
    }

    inc = (1 == inc) ? 1 : inc / 2;
  }

  return guess;
}

void
unset_multiples(struct bitvector *primes, size_t n, size_t max, int n_mod_6)
{
  size_t mult, inc_4, inc_2, max_idx;


  inc_4 = (n * (3 + n_mod_6) - 1) / 3;
  inc_2 = (n * (3 - n_mod_6) + 1) / 3;

  max_idx = (max - 1) / 3;

  mult = (n * n - 1) / 3 - 1;
  while (mult < max_idx)
  {
    bitvector_unset(primes, mult);
    mult += inc_4;

    if (mult >= max_idx)
    {
      break;
    }

    bitvector_unset(primes, mult);
    mult += inc_2;
  }
}
