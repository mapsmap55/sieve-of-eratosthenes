/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of sieve-of-eratosthenes.
 *
 * sieve-of-eratosthenes is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sieve-of-eratosthenes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sieve-of-eratosthenes.  If not, see
 * <https://www.gnu.org/licenses/>.
 */


#ifndef LIBSIEVE_SIEVE_H
# define LIBSIEVE_SIEVE_H


# include <stddef.h>


# ifdef __cplusplus
extern "C"
{
# endif


extern
char const * const
libsieve_version;

# define libsieve_header_version ("@PACKAGE_VERSION@")


int
sieve(size_t max, int (*process_prime)(size_t, void *), void *arg);


# ifdef __cplusplus
}
# endif


#endif
