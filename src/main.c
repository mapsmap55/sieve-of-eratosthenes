/* Copyright (C) 2019 Mike Petersen */
/*
 * This file is part of sieve-of-eratosthenes.
 *
 * sieve-of-eratosthenes is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sieve-of-eratosthenes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with sieve-of-eratosthenes.  If not, see
 * <https://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <sieve.h>


static
void
print_usage(FILE *out, char const *name);

static
void
print_version(FILE *out);

static
char *
size_t_to_string(size_t n, char buf[21]);

static
int
print_prime(size_t prime, void *arg);


/**********************
 * exported functions *
 **********************/

int
main(int argc, char **argv)
{
  size_t max;
  char *end;
  int ret;


  if (2 != argc)
  {
    print_usage(stderr, PACKAGE_NAME);

    return 1;
  }

  if (0 == strcmp("-?", argv[1]) || 0 == strcmp("-h", argv[1])
        || 0 == strcmp("--help", argv[1]))
  {
    print_usage(stdout, PACKAGE_NAME);

    return 0;
  }

  if (0 == strcmp("--version", argv[1]))
  {
    print_version(stdout);

    return 0;
  }

  max = strtoul(argv[1], &end, 10);
  if ('\0' == *argv[1] || '\0' != *end)
  {
    fprintf(stderr, "Unable to parse \"%s\" as a number.\n", argv[1]);

    return 2;
  }

  if (ERANGE == errno)
  {
    fprintf(stderr, "\"%s\" is too large a number.\n", argv[1]);

    return 2;
  }

  ret = sieve(max, &print_prime, stdout);

  if (ret)
  {
    fprintf(stderr, "Unable to run sieve.\n");

    return 3;
  }

  return 0;
}


/*******************
 * local functions *
 *******************/

void
print_usage(FILE *out, char const *name)
{
  fprintf(out, "Usage:\n");
  fprintf(out, "  %s <MAXIMUM>\n", name);
  fprintf(out, "    to enumerate primes up to MAXIMUM (inclusive)\n");
  fprintf(out, "  %s -?\n", name);
  fprintf(out, "  %s -h\n", name);
  fprintf(out, "  %s --help\n", name);
  fprintf(out, "    to show this help\n");
  fprintf(out, "  %s --version\n", name);
  fprintf(out, "    to display version information\n");
}

void
print_version(FILE *out)
{
  fprintf(out, "%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
  fprintf(out, "libsieve %s, compiled against %s\n", libsieve_version,
          libsieve_header_version);
  fprintf(out, "Copyright (C) 2019 Mike Petersen\n");
  fprintf(out, "License GPLv3+: GNU GPL version 3 or later.\n");
}

char *
size_t_to_string(size_t n, char buf[21])
{
  char *it;


  buf[20] = '\0';

  if (0 == n)
  {
    buf[19] = '0';

    return &buf[19];
  }

  it = &buf[20];
  while (0 != n)
  {
    --it;
    *it = '0' + n % 10;
    n /= 10;
  }

  return it;
}

int
print_prime(size_t prime, void *arg)
{
  char *str, buf[22];

  str = size_t_to_string(prime, buf);
  buf[20] = '\n';
  buf[21] = '\0';
  fputs(str, (FILE *) arg);

  return 0;
}
